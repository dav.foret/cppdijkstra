#pragma once
#include <vector>

class Edge;
class Vertex;
class DijkstraResult;

using Edge_vec = std::vector<Edge>;
using Vertex_vec = std::vector<Vertex>;
using Vertex_id = int;

/** \brief Bundles together the length of the shortest path to the dst and its length.
 */
class DijkstraResult {
    /// Edges of the shortest path.
    Edge_vec path_;
    /// Length of the shortest path.
    int length_;
};