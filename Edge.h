#pragma once
#include <vector>

class Edge;
class Vertex;
class DijkstraResult;
using Vertex_vec = std::vector<Vertex>;
using Vertex_id = int;

/** \brief Edge class.
 * Contains a Vertex& source, destination and integer length.
 */
class Edge {
public:
    /// Source vertex.
    Vertex_id src_;
    /// Destination vertex.
    Vertex_id dst_;
    /// Length of the edge.
    int len_;
    /** \brief Getter for source vertex.
     * @return Source vertex of the current edge.
     */
    Vertex_id get_src_() const { return src_; }



    Vertex_id get_dst_() const { return dst_; };

    int get_len_() const { return len_; };

    /** \brief Basic constructor.
     * @param src Source vertex.
     * @param dst Destination vertex.
     * @param len Length of the edge.
     */
     Edge(Vertex_id src, Vertex_id dst, int len) : src_(src),
                                                             dst_(dst),
                                                             len_(len) {};
};



