#pragma once

#include <vector>
#include "Edge.h"

/**
 *
 */
class EdgeVec {
public:
    std::vector<Edge> edges_;
    int vertex_count_;

    bool insert_edge_(Edge edge) { edges_.push_back(edge); };

    std::vector<Edge> edges() const { return edges_; };

    bool set_vertex_count_(int vertex_count) { vertex_count_ = vertex_count; }

    int get_vertex_count_() const { return vertex_count_; }
};