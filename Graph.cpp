#include "Graph.h"
#include "DijkstraResult.h"

Graph::Graph(EdgeVec edges) {
    vertices_.resize(edges.get_vertex_count_());
    // Processing the edges
    for(auto&& edge : edges.edges_) {
        vertices_.at(edge.get_src_()).attach_edge_(edge);
        vertices_.at(edge.get_dst_()).attach_edge_(edge);
    };
};

DijkstraResult Graph::dijkstra_(Vertex_id src, Vertex_id dst) {
    edge_queue_.push(Edge(0, 0, 0));
    while(!edge_queue_.empty()) {
        Edge current_edge = get_edge_queue_().front();
        Vertex& source = vertices_.at(current_edge.get_src_());
        Vertex& destination = vertices_.at(current_edge.get_dst_());
        Vertex& current_vertex = (source.is_open_() ? source : destination);
        // Try to add all edges to the queue
        while (current_vertex.is_open_()) {
            auto& curr_edge = current_vertex.get_edge_();
            auto& src_vertex = vertices_.at(curr_edge.get_src_());
            auto& dst_vertex = vertices_.at(curr_edge.get_dst_());
            // Choose only unvisited edges as every edge will be tried twice
            if(src_vertex.is_open_() && dst_vertex.is_open_()) {
                // Modify distance of the destination and push the edge /if/ the resulting distance would be less
                if((src_vertex.get_dist_() + curr_edge.get_len_() < dst_vertex.get_dist_()) || (dst_vertex.get_dist_() == -1)) {
                    edge_queue_.push(curr_edge);
                    dst_vertex.set_dist_(src_vertex.get_dist_() + curr_edge.get_len_());
                    dst_vertex.set_lst_path_(curr_edge.get_src_());
                };
            };
        };
    };
}

const Vertex_vec &Graph::get_vertices_() const {
    return vertices_;
}

const std::queue<Edge> &Graph::get_edge_queue_() const {
    return edge_queue_;
};
