#pragma once
#include "Edge.h"
#include "Vertex.h"
#include "EdgeVec.h"
#include <vector>
#include <queue>

class DijkstraResult;
using Vertex_vec = std::vector<Vertex>;
using Vertex_id = int;

 /** \brief A graph class.
 *		    Contains edges and vertices.
 *
 */
class Graph {
public:
	/// Vertex container.
	Vertex_vec vertices_;
	/// Stores the edges in an ascending order, roughly 'sorted' by depth, but not necessarily distance.
    std::queue<Edge> edge_queue_;
	/** \brief Transforms a container of edges into a graph.
	 * @param edges Edge list
	 */
	 // TODO: Implement in the source file.
	Graph(EdgeVec edges);
    /** \brief
     *
     * @param src
     * @param dst
     * @return
     */
	DijkstraResult dijkstra_(Vertex_id src, Vertex_id dst);

    const Vertex_vec &get_vertices_() const;

    const std::queue<Edge>& get_edge_queue_() const;
};