#pragma once
#include <string>
#include <iostream>

/** \brief Base class for task-specific derived LoaderX classes.
 */
class Loader {
public:
    /// Basic info about input formatting.
    std::string _config_line;
    /** \brief Handles parsing of the input stream.
     */
    virtual void load_data_() = 0;
    /** \brief Stores data.
     *
     */
     // TODO: Documentation
     inline void store_config_() {
         std::getline(std::cin, _config_line);
     };
};


