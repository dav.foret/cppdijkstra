#pragma once
#include <vector>
#include "Loader.h"
#include "Edge.h"
#include "EdgeVec.h"
#include <sstream>
#include <iostream>

class Edge;
class Vertex;
class DijkstraResult;
using Vertex_vec = std::vector<Vertex>;
using Vertex_id = int;

/** \brief Specialized loader for Dijkstra.
 */
class LoaderDijkstra : public Loader {
public:
    /// Expected amount of edges.
    int _edge_count;
    /// Container object for edges this class outputs.
    EdgeVec edges_;
    LoaderDijkstra() { Loader::store_config_(); };
    /** \brief Getter for loaded edges.
     * @return Ref. to the edges member.
     */
    EdgeVec get_edges_() const { return edges_; };
    /** \brief Processes input line by line into an Edge_vec object.
     */
    void load_data_() override {
        std::basic_stringstream<char>(_config_line) >> _edge_count;
        std::string line;
        int max_label_ = -1;
        // Creates edges from the supplied string.
        while(std::getline(std::cin, line)) {
            std::basic_stringstream<char> edge_stream(line);
            int src, dst, len;
            edge_stream >> src;
            edge_stream >> dst;
            edge_stream >> len;
            while(src > max_label_ || dst > max_label_) { ++max_label_; };
            Edge new_edge(src, dst, len);
            edges_.insert_edge_(new_edge);
            edges_.set_vertex_count_(max_label_ + 1);
        };
    };
};