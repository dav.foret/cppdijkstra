#pragma once
#include <vector>
#include "EdgeVec.h"

class Edge;
class Vertex;
class DijkstraResult;
using Vertex_vec = std::vector<Vertex>;
using Vertex_id = int;


 /** \brief Vertex class.
 *		    Contains known least distance path value, pointers to all linked vertices and
 *		    least distance path edge ref..
 */
class Vertex {
public:
    /// The least known distance from starting vertex, none if -1.
    int dist_ = -1;
    ///
    bool open_ = true;
    /// The vertex that is a part of the current least known path (-1 means this vertex hasn't been visited yet.
    Vertex_id lst_path_ = -1;
    /// Ids of connected vertices.
   std::vector<Edge> edges_;
    bool is_open_() const { return open_; };
    /** \brief Add an edge id for the algorithm.
     *
     */
     void attach_edge_(Edge edge) { edges_.push_back(edge); };

     Edge& get_edge_() {
         if(edges_.size()) {
            open_ = false;
         };
         auto& ret = edges_.back();
         edges_.pop_back();
         return ret;
     };
     bool is_open_() { return open_; };

     void set_dist_(int dist) { dist_ = dist; };
     int get_dist_() const { return dist_; };
     void set_lst_path_(Vertex_id lst_path) { lst_path_ = lst_path; };
};
